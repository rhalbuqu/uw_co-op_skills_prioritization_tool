<?php

/**
 * @file
 * Customize confirmation screen after successful submission.
 *
 * This file may be renamed "webform-confirmation-[nid].tpl.php" to target a
 * specific webform e-mail on your site. Or you can leave it
 * "webform-confirmation.tpl.php" to affect all webform confirmations on your
 * site.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $progressbar: The progress bar 100% filled (if configured). This may not
 *   print out anything if a progress bar is not enabled for this node.
 * - $confirmation_message: The confirmation message input by the webform
 *   author.
 * - $sid: The unique submission ID of this submission.
 * - $url: The URL of the form (or for in-block confirmations, the same page).
 */
?>

<?php
/**
 * Include constant definitions
 * This section includes the file that holds all the constant definitions.
 */

/**
 * Helper function for getting a random element in an array.
 *
 * @param $list
 *   An array which holds the list of elements to be chosen from
 *
 * @return string
 */
  function get_random_element($list) {
    $min = 0;
    $max = count($list) - 1;
    $index = rand($min, $max);
    $retval = $list[$index];
    return $retval;
  }

/**
 * Helper function that checks if a string contains a number. Returns false
 * otherwise.
 *
 * @param $string
 *
 * @return bool
 */
  function has_number($string) {
    $len = drupal_strlen($string);
    for ($i = 0; $i < $len; $i++) {
      if(ctype_digit($string[$i])) {
        return TRUE;
      }
    }
    return FALSE;
  }

/**
 * Helper function to check if string is a course code
 * This assumes all course code will always start with a letter and end with either
 * a number or an uppercase letter. Furthermore, every course code will have at
 * least one number.
 *
 * @param $string
 *
 * @return bool
 */
  function is_course($string) {
    $first = $string[0];
    $last = $string[drupal_strlen($string) - 1];

    if (has_number($string) && ctype_alpha($first) && (ctype_digit($last) || ctype_upper($last))) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

/**
 * Helper function to check if a string is a PD course
 * This assumes that every PD course will start with "PD" and the last character
 * of the string is a number.
 *
 * @param $string
 *
 * @return bool
 */
  function is_pd_course($string) {
    $pd = $string[0] . $string[1];
    $last = $string[drupal_strlen($string) - 1];
    return (ctype_digit($last) && $pd == "PD");
  }

/**
 * Helper function to check if position is a Don position.
 *
 * @param $string
 *
 * @return bool
 */
  function is_don_position($string) {
    $length = drupal_strlen($string);
    $word = "";
    for ($i = 4; $i > 0; $i--) {
      $word .= $string[$length - $i];
    }
    return ($word == " Don" || $word == " don");
  }

/**
 * Helper function which filters the list into a list with only one Don position.
 *
 * @param $list
 *
 * @return array
 */
  function filter_don_positions($list) {
    $length = count($list);

    for ($i = 0; $i < $length; $i++) {
      if (is_don_position($list[$i])) {
        unset($list[$i]);
        // Reindex the keys in array.
        $list = array_values($list);
        $length--;
      }
    }
    return $list;
  }

/**
 * Helper function which filters the list into a list with only one Don position.
 *
 * @param $list
 *
 * @return array
 */
  function filter_don($list) {
    $length = count($list);

    for ($i = 0; $i < $length; $i++) {
      if (is_don_position($list[$i]->result)) {
        unset($list[$i]);
        // Reindex the keys in array.
        $list = array_values($list);
        $length--;
      }
    }
    return $list;
  }

/**
 * Cross checks submission values with the given rule.
 *
 * @param $data
 * @param $rule
 *
 * @return bool
 */
  function rule_met($data, $rule) {
    $length = count($data);
    // Reindex array.
    $data = array_values($data);
    for ($i = 0; $i < $length; $i++) {
      if (isset($data[$i])) {
        $number_of_selections = count($data[$i]);
        for ($j = 0; $j < $number_of_selections; $j++) {
          if(in_array($data[$i][$j], $rule)) {
            $index = array_search($data[$i][$j], $rule);
            unset($rule[$index]);
            // Reindex the keys in array.
            $rule = array_values($rule);
          }
        }
      }
    }
    return empty($rule);
  }

/**
 * Obtains the 1st component of the EDGE path.
 *
 * @param $submission
 *
 * @return string
 */
  function get_high_interest($submission) {
    $query = db_select('find_your_edge_rulesets', 'fyer')
      ->fields('fyer')
      ->condition('component', '1')
      ->execute()
      ->fetchAll();

    $length = count($query);
    $results = array();
    for ($i = 0; $i < $length; $i++) {
      $rule = explode(',', str_replace(' ', '', $query[$i]->rule));
      if ($rule[0] == 'NORULE' || rule_met($submission->data, $rule)) {
        $ruleset = new stdClass();
        $ruleset->result = $query[$i]->result;
        $ruleset->description = $query[$i]->description;
        $ruleset->url = $query[$i]->url;
        if (!in_array($ruleset, $results)) {
          $results[] = $ruleset;
        }
      }
    }

    if(empty($results)) {
      $ruleset = new stdClass();
      $ruleset->result = "You don't have major interest in any of the skills.";
      $results[] = $ruleset;
    }
    return ($results);
  }

/**
 * Obtains the 2nd component of the EDGE path.
 *
 * @param $submission
 *
 * @return string
 */
  function get_medium_interest($submission) {
    $query = db_select('find_your_edge_rulesets', 'fyer')
      ->fields('fyer')
      ->condition('component', '2')
      ->execute()
      ->fetchAll();

    $length = count($query);
    $results = array();
    for ($i = 0; $i < $length; $i++) {
      $rule = explode(',', str_replace(' ', '', $query[$i]->rule));
      if ($rule[0] == 'NORULE' || rule_met($submission->data, $rule)) {
        $ruleset = new stdClass();
        $ruleset->result = $query[$i]->result;
        $ruleset->description = $query[$i]->description;
        $ruleset->url = $query[$i]->url;
        if (!in_array($ruleset, $results)) {
          $results[] = $ruleset;
        }
      }
    }
    if(empty($results)) {
      $ruleset = new stdClass();
      $ruleset->result = "Career Course";
      $ruleset->description = "The career development course will help you develop core career-seeking skills like résumé writing and interviewing. Click the box to learn more about the career development course.";
      $ruleset->url = "https://uwaterloo.ca/edge/career-development-course";
      $results[] = $ruleset;
    }
        return ($results);
  }

/**
 * Obtains the 3rd component of the EDGE path.
 * Note: Only 1 Don position can be used as an experience at a time.
 * Furthermore, once a specific on-campus experience has been found, its
 * corresponding general result will be removed from the list of possible results.
 *
 * @param $submission
 *
 * @return array
 */
  function get_low_interest($submission) {
    $query = db_select('find_your_edge_rulesets', 'fyer')
      ->fields('fyer')
      ->condition('component', '3')
      ->execute()
      ->fetchAll();

    $faculties = db_select('find_your_edge_rulesets', 'fyer')
      ->fields('fyer', array('result'))
      ->condition('rule', '%FAC%', 'LIKE')
      ->condition('result', array('Faculties'), 'NOT IN')
      ->execute()
      ->fetchCol();

    $university_colleges = db_select('find_your_edge_rulesets', 'fyer')
      ->fields('fyer', array('result'))
      ->condition('rule', '%UC%', 'LIKE')
      ->condition('result', array('University colleges'), 'NOT IN')
      ->execute()
      ->fetchCol();

    $student_society = db_select('find_your_edge_rulesets', 'fyer')
      ->fields('fyer', array('result'))
      ->condition('rule', '%SSOC%', 'LIKE')
      ->condition('result', array('Student societies'), 'NOT IN')
      ->execute()
      ->fetchCol();

    $clubs = db_select('find_your_edge_rulesets', 'fyer')
      ->fields('fyer', array('result'))
      ->condition('rule', '%CLUBS%', 'LIKE')
      ->condition('result', array('Feds clubs'), 'NOT IN')
      ->execute()
      ->fetchCol();

    $housing = db_select('find_your_edge_rulesets', 'fyer')
      ->fields('fyer', array('result'))
      ->condition('rule', '%HOUSEATHL%', 'LIKE')
      ->condition('result', array('Student services'), 'NOT IN')
      ->execute()
      ->fetchCol();

    $feds = db_select('find_your_edge_rulesets', 'fyer')
      ->fields('fyer', array('result'))
      ->condition('rule', '%FEDS%', 'LIKE')
      ->condition('result', array('Feds services'), 'NOT IN')
      ->execute()
      ->fetchCol();

    $has_faculty = FALSE;
    $has_university_college = FALSE;
    $has_student_society = FALSE;
    $has_clubs = FALSE;
    $has_housing = FALSE;
    $has_feds = FALSE;

    $length = count($query);
    $results = array();
    for ($i = 0; $i < $length; $i++) {
      $rule = explode(',', str_replace(' ', '', $query[$i]->rule));
      if ($rule[0] == 'NORULE' || rule_met($submission->data, $rule)) {
        $ruleset = new stdClass();
        $ruleset->result = $query[$i]->result;
        $ruleset->description = $query[$i]->description;
        $ruleset->url = $query[$i]->url;
        if (!in_array($ruleset, $results)) {
          $results[] = $ruleset;
        }
        // Check if we've found a specific result.
        if(in_array($ruleset->result, $faculties)) {
          $has_faculty = TRUE;
        }
        elseif(in_array($ruleset->result, $university_colleges)) {
          $has_university_college = TRUE;
        }
        elseif(in_array($ruleset->result, $student_society)) {
          $has_student_society = TRUE;
        }
        elseif(in_array($ruleset->result, $clubs)) {
          $has_clubs = TRUE;
        }
        elseif(in_array($ruleset->result, $housing)) {
          $has_housing = TRUE;
        }
        elseif(in_array($ruleset->result, $feds)) {
          $has_feds = TRUE;
        }
      }
    }//End for loop

    // If we've gotten a specific result, then unset the general result.
    $results_length = count($results);
    if($has_faculty) {
      for ($i = 0; $i < $results_length; $i++) {
        if ($results[$i]->result == 'Faculties') {
          unset($results[$i]);
          // Reindex the keys in array.
          $results = array_values($results);
          $results_length--;
          break;
        }
      }
    }
    if($has_university_college) {
      for ($i = 0; $i < $results_length; $i++) {
        if ($results[$i]->result == 'University colleges') {
          unset($results[$i]);
          // Reindex the keys in array.
          $results = array_values($results);
          $results_length--;
          break;
        }
      }
    }
    if($has_student_society) {
      for ($i = 0; $i < $results_length; $i++) {
        if ($results[$i]->result == 'Student societies') {
          unset($results[$i]);
          // Reindex the keys in array.
          $results = array_values($results);
          $results_length--;
          break;
        }
      }
    }
    if($has_housing) {
      for ($i = 0; $i < $results_length; $i++) {
        if ($results[$i]->result == 'Student services') {
          unset($results[$i]);
          // Reindex the keys in array.
          $results = array_values($results);
          $results_length--;
          break;
        }
      }
    }
    if($has_clubs) {
      for ($i = 0; $i < $results_length; $i++) {
        if ($results[$i]->result == 'Feds clubs') {
          unset($results[$i]);
          // Reindex the keys in array.
          $results = array_values($results);
          $results_length--;
          break;
        }
      }
    }
    if($has_feds) {
      for ($i = 0; $i < $results_length; $i++) {
        if ($results[$i]->result == 'Feds services') {
          unset($results[$i]);
          // Reindex the keys in array.
          $results = array_values($results);
          $results_length--;
          break;
        }
      }
    }

    // If we're unable to obtain 3 experiences, fill in the rest with "Other Experiences".
    $return_list = array();
    if (count($results) < 1) {
      while (count($results) < 1) {
        $ruleset = new stdClass();
        $ruleset->result = "No particular skills to display";
        // No lines breaks since it will be picked up when generating the PDF.
        $ruleset->description = "Any skill can be devoloped";
        $ruleset->url = "";
        $results[] = $ruleset;
      }
      $return_list = $results;
    }
    else {
      for ($i = 0; $i < 3; $i++) {
        $return_list[] = get_random_element($results);
        // Delete from array to avoid duplicates.
        $index = array_search($return_list[$i], $results);
        unset($results[$index]);
        // Reindex the keys in array.
        $results = array_values($results);

        // If we've found a Don position, filter out the rest so we can't get another
        // This should only be true at most ONCE.
        if (is_don_position($return_list[$i]->result)) {
          $results = filter_don($results);
        }
      }
    }
    return $return_list;
  }

/**
 * Obtains the PD course for the EDGE path.
 *
 * @param $submission
 *
 * @return string
 */
  function get_component_pd($submission) {
    $query = db_select('find_your_edge_rulesets', 'fyer')
      ->fields('fyer')
      ->condition('component', 'pd')
      ->execute()
      ->fetchAll();

    $length = count($query);
    $results = array();
    for ($i = 0; $i < $length; $i++) {
      $rule = explode(',', str_replace(' ', '', $query[$i]->rule));
      if ($rule[0] == 'NORULE' || rule_met($submission->data, $rule)) {
        $ruleset = new stdClass();
        $ruleset->result = $query[$i]->result;
        $ruleset->description = $query[$i]->description;
        $ruleset->url = $query[$i]->url;
        if (!in_array($ruleset, $results)) {
          $results[] = $ruleset;
        }
      }
    }

    if(empty($results)) {
      $ruleset = new stdClass();
      $ruleset->result = "PD Course";
      $ruleset->description = "You can develop your professional skills by completing a PD course in tandem with an on- or off-campus experience. Click the box to learn more about PD courses.";
      $ruleset->url = "https://uwaterloo.ca/professional-development-program/courses";
      $results[] = $ruleset;
    }

    return get_random_element($results);
  }

/**
 * Provides the start <a> tag if $string is not "Other Experience".
 *
 * @param $string
 * @param $link
 */
  function gen_href_start($string, $link) {
    if ($string != "Other Experience") {
      print '<a href="' . $link . '" target="_blank">';
    }
  }

/**
 * Provides the end <a> tag if $string is not "Other Experience".
 *
 * @param $string
 */
  function gen_href_end($string) {
    if ($string != "Other Experience") {
      print '</a>';
    }
  }

/**
 * Process the string so that it can properly added to a pdf template.
 *
 * @param $string
 *
 * @return mixed
 */
  function pdf_process($key, $string) {
    $value = preg_replace('/<a href=.*/', '', $string);
    if((is_course($key) && !is_pd_course($key)) || $key == "PD1") {
      $value .= 'EDGE courses.';
    }
    elseif ($key == "Other Experience") {
      $value .= 'types of EDGE experiences to learn more about the criteria for this milestone.';
    }
    return $value;
  }

/**
 * Write to database so pdf can grab information when trying to generate.
 *
 * @param $sid
 * @param $component1
 * @param $component2
 * @param $component3
 * @param $component_pd
 */
  function pdf_store_results($sid, $component1, $component2, $component3, $component_pd) {
    $result_db = new stdClass();
    $result_db->sid = $sid;
    $result_db->component1 = $component1->result;
    $result_db->component1_descr = pdf_process($component1->result, $component1->description);
    $result_db->component2 = $component2->result;
    $result_db->component2_descr = pdf_process($component2->result, $component2->description);
    $result_db->component3a = $component3[0]->result;
    $result_db->component3a_descr = pdf_process($component3[0]->result, $component3[0]->description);
    $result_db->component3b = $component3[1]->result;
    $result_db->component3b_descr = pdf_process($component3[1]->result, $component3[1]->description);
    $result_db->component3c = $component3[2]->result;
    $result_db->component3c_descr = pdf_process($component3[2]->result, $component3[2]->description);
    $result_db->component3_pd = $component_pd->result;
    $result_db->component3_pd_descr = pdf_process($component_pd->result, $component_pd->description);
    $query = db_select('find_your_edge_results', 'fyer')
      ->fields('fyer')
      ->condition('sid', $sid)
      ->execute()
      ->fetchAll();

    if(count($query) == 0) {
      drupal_write_record('find_your_edge_results', $result_db);
    }
    else{
      drupal_write_record('find_your_edge_results', $result_db, 'sid');
    }
  }

  $sid = $_GET['sid'];
  $submission = webform_get_submission($node->nid, $sid);
  $access_token = token_replace('[submission:access-token]', array('webform-submission' => $submission));

  $highinterest = get_high_interest($submission);
  $mediuminterest = get_medium_interest($submission);
  $lowinterest = get_low_interest($submission);

  //pdf_store_results($sid, $component1, $component2, $component3, $component_pd_)
?>

<div class="flex-container">

        <h1>Employability Skills Scorecard</h1>


    <div class="flex-message">
        <p> We’ve created the following rankings based on your answers to the six scenarios. If you have any questions regarding this process or the tool in general, contact
            <a href="mailto:abrunet@uwaterloo.ca">Andrew Brunet</a>.
        </p>
    </div>

    <div>
        <p class="caption"> <strong> *Consider </strong> which of these skills you would like to learn more about or develop.
            Then click below to view a PDF version of this scorecard. This will be useful to build your Co-op Skills Development Plan, which
            helps find campus services most relevant to you.</p>
    </div>

    <hr />

    <h2>High Interest</h2>

        <?php
        $sizeofhighinterest = count($highinterest);
        for($i = 0; $i < $sizeofhighinterest; $i++) {
            print '<div class="component_square">
                      <div class="call-to-action-center-wrapper">' ;
            gen_href_start($highinterest[$i]->result,$highinterest[$i]->url);
            print          '<div class="call-to-action-theme-science">
                                <div class="call-to-action-big-text">';
            print              $highinterest[$i]->result;
            print'                   </div>
                                 </div>';
            gen_href_end($highinterest[$i]->result);
            print '           </div>
                           </div>
                       </div>';
            print     '<div class="flex-component-description">
                       <p class="p1">';
            print    $highinterest[$i]->description;
        }
        ?>

    <hr />

    <h2>Medium Interest</h2>

    <div class="flex-component-block">
        <?php
        $sizeofmediuminterest = count($mediuminterest);
        for($i = 0; $i < $sizeofmediuminterest; $i++) {
            print '<div class="component_square">
                <div class="call-to-action-center-wrapper">' ;
            gen_href_start($mediuminterest[$i]->result, $mediuminterest[$i]->url) ;
            print       '<div class="call-to-action-theme-uWaterloo">
                             <div class="call-to-action-big-text">' ;
            print              $mediuminterest[$i]->result;
            print '            </div>
                        </div>';
            gen_href_end($mediuminterest[$i]->result);
            print '    </div>
               </div>
             </div>';
            print          '<div class="flex-component-description">
                                 <p class="p1">';
            print              $highinterest[$i]->description;
        }
        ?>
    </div>


    <hr />

    <h2>Low Interest</h2>


     <div class="flex-component-block">
        <?php
         $sizeoflowinterest = count($lowinterest);
         for($i = 0; $i < $sizeoflowinterest; $i++) {
            print '<div class="component_square">
                <div class="call-to-action-center-wrapper">' ;
        gen_href_start($lowinterest[$i]->result, $lowinterest[$i]->url) ;
        print     '<div class="call-to-action-theme-arts">
                        <div class="call-to-action-big-text">' ;
        print              $lowinterest[$i]->result;
        print '            </div>
                 </div>';
        gen_href_end($lowinterest[$i]->result);
        print '    </div>
               </div>
             </div>';
             print          '<div class="flex-component-description">
                                 <p class="p1">';
             print              $highinterest[$i]->description;

    }
    ?>
</div>

<hr />

    <div class="flex-back-button-wrapper">
        <div id ="-button" class="edge-action-button-wrapper adjust-height">
            <div class="call-to-action-wrapper">
    <p> Now that you're done reviewing, select one of the options below:
        <?php
        $nid = variable_get('uw_find_your_edge_nid', 0);
        if(isset($submission->data[1][0]) && $submission->data[1][0] == 1) {
            $fid = variable_get('uw_find_your_edge_fid_international', 0);
            $url = fillpdf_pdf_link($form_id = $fid, $node_id = $nid);
            print '<a href=' . $url . '&webform[sid]=' . $sid . '&sid=' . $sid . '&token=' . $access_token . '></a>';
        }
        else {
            $fid = variable_get('uw_find_your_edge_fid_regular', 0);
            $url = fillpdf_pdf_link($form_id = $fid, $node_id = $nid);
            print '<a href=' . $url . '&webform[sid]=' . $sid . '&sid=' . $sid . '&token=' . $access_token . '></a>';
        }
        ?>
    </p>
                <div class="call-to-action-wrapper adjust-height">
                    <div class="call-to-action-theme-uWaterloo">
                        <div class="call-to-action-big-text">
                            <?php print t("Save PDF of this Scorecard?") ?>
                        </div>
                    </div>
                </div>
</div>
            &nbsp;
<div class="flex-back-button-wrapper">
    <div id ="program-search-submitbutton" class="form-submit">
        <div class="call-to-action-wrapper">
            <?php
            $nid = variable_get('uw_find_your_edge_nid', 0);
            $url = url('/node/' . $nid);
            print '<a href="' . $url . '">';
            ?>
            <div class="call-to-action-wrapper adjust-height">
                <div class="edge-action-button-gray">
                    <div class="call-to-action-big-text">
                        <?php print t("Restart the tool") ?>
                    </div>
                </div>
            </div>
            </a>
        </div>
    </div>
</div>

</div>

